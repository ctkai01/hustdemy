FROM node:20-alpine as build

WORKDIR /app
ENV STAGE=prod

RUN apk update && apk add --no-cache \
    apparmor \
    apparmor-profiles \
    apparmor-utils \
    tzdata \
    ffmpeg
COPY package*.json ./
RUN npm ci --force

COPY --chown=node:node . .
RUN npm run build

# Running `npm ci` removes the existing node_modules directory and passing in --only=production ensures that only the production dependencies are installed. This ensures that the node_modules directory is as optimized as possible
RUN npm ci --force --only=production && npm cache clean --force

USER node

FROM node:20-alpine

WORKDIR /app
RUN apk update && apk add --no-cache \
    ffmpeg \ 
    tzdata
COPY --from=build --chown=node:node /app/package*.json ./
COPY --from=build --chown=node:node /app/node_modules ./node_modules
COPY --from=build --chown=node:node /app/dist ./dist

CMD ["node", "dist/main.js"]