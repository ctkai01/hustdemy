import { Course, Level, Price, User } from 'src/entities';

export const FEE_PLATFORM = 10;

export interface CartItem {
  cartID: number;
  course: CartItemCourse;
}

export interface CartResponse {
  userID: number;
  cartItems: CartItem[];
}

export interface CartItemCourse {
  id: number;
  image: string;
  level: Level;
  price: Price;
  title: string;
  subtitle: string;
  createdAt: Date;
  user: User;
  description: string;
  duration: number;
  lectureCount: number;
  countReview: number;
  averageReview: number;
}
