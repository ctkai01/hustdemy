import { IsEnum } from 'class-validator';
import { NotificationType } from 'src/constants';

export class ReadNotificationDto {
  @IsEnum(NotificationType)
  type: NotificationType;
}
