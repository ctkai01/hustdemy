import { IsOptional, IsString } from 'class-validator';
import { PageCommonOptionsDto } from 'src/common/paginate/page-option.dto';

export class GetNotificationDto extends PageCommonOptionsDto {
    @IsOptional()
    @IsString()
    type: string
}
