import { Module } from "@nestjs/common";
import StripeService from "./stripe.service";
import StripeWebhookController from "./stripeWebhook.controller";
import { UserService } from "../user/user.service";

@Module({
  imports: [UserService],
  providers: [StripeService],
  controllers: [StripeWebhookController],
})
export class StripeWebhookModule {}
